# Group Commerce

Group Commerce provides the ability to create group relationships to commerce
entities, including stores, orders, products, profiles, and promotions.

## REQUIREMENTS

This module requires the following modules:

- [Commerce](https://drupal.org/project/commerce)
- [Group](https://drupal.org/project/group)

## INSTALLATION

Install as you would normally install a contributed Drupal module. Visit
https://www.drupal.org/docs/extending-drupal/installing-modules for
further information.

## CONFIGURATION

## KNOWN ISSUES AND LIMITATIONS

[Issue Tracker](https://www.drupal.org/project/issues/gcommerce?version=3.x)
