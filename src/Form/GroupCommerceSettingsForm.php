<?php

namespace Drupal\gcommerce\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\group\Entity\GroupType;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The Group commerce settings form.
 */
class GroupCommerceSettingsForm extends ConfigFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['gcommerce.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'gcommerce_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $config = $this->config('gcommerce.settings');
    $form['gcommerce_allowed_group_types_wrapper'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Group type'),
    ];
    $form['gcommerce_allowed_group_types_wrapper']['description'] = [
      '#type' => 'item',
      '#markup' => $this->t('<p>Specify the group type(s) that integrate with group commerce. This will typically be the default "Company" group type, unless you are implementing custom group types. This is currently used for auto-adding carts(orders) and profiles to groups.</p>'),
    ];
    $group_types = GroupType::loadMultiple();
    $options = [];
    foreach ($group_types as $key => $group_type) {
      $options[$key] = $group_type->label();
    }
    $form['gcommerce_allowed_group_types_wrapper']['allowed_group_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Groups that should get auto-assigned orders and/or profiles.'),
      '#options' => $options,
      '#required' => TRUE,
      '#default_value' => $config->get('allowed_group_types') ?? [],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $config = $this->config('gcommerce.settings');
    $config->set('allowed_group_types', array_filter($form_state->getValue('allowed_group_types')));
    $config->save();
  }

}
