<?php

namespace Drupal\gcommerce_product\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Subscriber for Group Commerce Product routes.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('entity.group_relationship.create_page')) {
      $copy = clone $route;
      $copy->setPath('group/{group}/products/create');
      $copy->setDefault('base_plugin_id', 'group_product');
      $collection->add('entity.group_relationship.group_product_create_page', $copy);
    }
    if ($route = $collection->get('entity.group_relationship.add_page')) {
      $copy = clone $route;
      $copy->setPath('group/{group}/products/add');
      $copy->setDefault('base_plugin_id', 'group_product');
      $collection->add('entity.group_relationship.group_product_add_page', $copy);
    }
  }

}
