<?php

namespace Drupal\Tests\gcommerce_product\Kernel;

use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;

/**
 * Tests that all config provided by this module passes validation.
 *
 * @group gcommerce_product
 */
class GroupProductConfigTest extends EntityKernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'address',
    'commerce',
    'commerce_product',
    'commerce_price',
    'commerce_store',
    'entity',
    'flexible_permissions',
    'gcommerce',
    'gcommerce_product',
    'group',
    'options',
    'variationcache',
    'views',
  ];

  /**
   * Tests that the module's config installs properly.
   */
  public function testConfig(): void {
    $this->installConfig(['gcommerce_product']);
  }

}
