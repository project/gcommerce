<?php

namespace Drupal\Tests\gcommerce_product\Functional;

use Drupal\Tests\group\Functional\EntityOperationsTest as GroupEntityOperationsTest;

/**
 * Tests that entity operations (do not) show up on the group overview.
 *
 * @see gcommerce_product_entity_operation()
 *
 * @group gcommerce_product
 */
class EntityOperationsTest extends GroupEntityOperationsTest {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['gcommerce_product'];

  /**
   * {@inheritdoc}
   */
  public function provideEntityOperationScenarios() {
    $scenarios['withoutAccess'] = [
      [],
      ['group/1/products' => 'Products'],
    ];

    $scenarios['withAccess'] = [
      ['group/1/products' => 'Products'],
      [],
      [
        'view group',
        'access group_product overview',
      ],
    ];

    $scenarios['withAccessAndViews'] = [
      ['group/1/products' => 'Products'],
      [],
      [
        'view group',
        'access group_product overview',
      ],
      ['views'],
    ];

    return $scenarios;
  }

}
