<?php

namespace Drupal\Tests\gcommerce_promotion\Functional;

use Drupal\Tests\group\Functional\EntityOperationsTest as GroupEntityOperationsTest;

/**
 * Tests that entity operations (do not) show up on the group overview.
 *
 * @see gcommerce_promotion_entity_operation()
 *
 * @group gcommerce_promotion
 */
class EntityOperationsTest extends GroupEntityOperationsTest {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['gcommerce_promotion'];

  /**
   * {@inheritdoc}
   */
  public function provideEntityOperationScenarios() {
    $scenarios['withoutAccess'] = [
      [],
      ['group/1/promotions' => 'Promotions'],
    ];

    $scenarios['withAccess'] = [
      ['group/1/promotions' => 'Promotions'],
      [],
      [
        'view group',
        'access group_promotion overview',
      ],
    ];

    $scenarios['withAccessAndViews'] = [
      ['group/1/promotions' => 'Promotions'],
      [],
      [
        'view group',
        'access group_promotion overview',
      ],
      ['views'],
    ];

    return $scenarios;
  }

}
