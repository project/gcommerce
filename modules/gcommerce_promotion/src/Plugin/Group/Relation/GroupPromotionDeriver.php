<?php

namespace Drupal\gcommerce_promotion\Plugin\Group\Relation;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\group\Plugin\Group\Relation\GroupRelationTypeInterface;

/**
 * Provides a deriver for group_promotion.
 */
class GroupPromotionDeriver extends DeriverBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    assert($base_plugin_definition instanceof GroupRelationTypeInterface);
    $this->derivatives = [];

    $name = 'commerce_promotion';
    $this->derivatives[$name] = clone $base_plugin_definition;
    $this->derivatives[$name]->set('entity_bundle', NULL);
    $this->derivatives[$name]->set('label', $this->t('Group commerce promotion'));
    $this->derivatives[$name]->set('description', $this->t('Adds commerce promotions to groups both publicly and privately.'));

    return $this->derivatives;
  }

}
