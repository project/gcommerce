<?php

namespace Drupal\gcommerce_promotion\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Subscriber for Group Commerce Promotion routes.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('entity.group_relationship.create_page')) {
      $copy = clone $route;
      $copy->setPath('group/{group}/promotions/create');
      $copy->setDefault('base_plugin_id', 'group_promotion');
      $collection->add('entity.group_relationship.group_promotion_create_page', $copy);
    }

    if ($route = $collection->get('entity.group_relationship.add_page')) {
      $copy = clone $route;
      $copy->setPath('group/{group}/promotions/add');
      $copy->setDefault('base_plugin_id', 'group_promotion');
      $collection->add('entity.group_relationship.group_promotion_add_page', $copy);
    }
  }

}
