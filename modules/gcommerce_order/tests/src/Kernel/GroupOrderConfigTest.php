<?php

namespace Drupal\Tests\gcommerce_order\Kernel;

use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;

/**
 * Tests that all config provided by this module passes validation.
 *
 * @group gcommerce_order
 */
class GroupOrderConfigTest extends EntityKernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'address',
    'commerce',
    'commerce_order',
    'commerce_price',
    'commerce_store',
    'entity',
    'entity_reference_revisions',
    'flexible_permissions',
    'gcommerce',
    'gcommerce_order',
    'group',
    'options',
    'profile',
    'state_machine',
    'variationcache',
    'views',
  ];

  /**
   * Tests that the module's config installs properly.
   */
  public function testConfig(): void {
    $this->installConfig(['gcommerce_order']);
  }

}
