<?php

namespace Drupal\Tests\gcommerce_order\Functional;

use Drupal\Tests\group\Functional\EntityOperationsTest as GroupEntityOperationsTest;

/**
 * Tests that entity operations (do not) show up on the group overview.
 *
 * @see gcommerce_order_entity_operation()
 *
 * @group gcommerce_order
 */
class EntityOperationsTest extends GroupEntityOperationsTest {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['gcommerce_order'];

  /**
   * {@inheritdoc}
   */
  public function provideEntityOperationScenarios() {
    $scenarios['withoutAccess'] = [
      [],
      ['group/1/orders' => 'Orders'],
    ];

    $scenarios['withAccess'] = [
      ['group/1/orders' => 'Orders'],
      [],
      [
        'view group',
        'access group_order overview',
      ],
    ];

    $scenarios['withAccessAndViews'] = [
      ['group/1/orders' => 'Orders'],
      [],
      [
        'view group',
        'access group_order overview',
      ],
      ['views'],
    ];

    return $scenarios;
  }

}
