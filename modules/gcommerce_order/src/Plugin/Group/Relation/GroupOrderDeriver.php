<?php

namespace Drupal\gcommerce_order\Plugin\Group\Relation;

use Drupal\commerce_order\Entity\OrderType;
use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\group\Plugin\Group\Relation\GroupRelationTypeInterface;

/**
 * Provides a deriver for group_order.
 */
class GroupOrderDeriver extends DeriverBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    assert($base_plugin_definition instanceof GroupRelationTypeInterface);
    $this->derivatives = [];

    foreach (OrderType::loadMultiple() as $name => $entity_type) {
      $label = $entity_type->label();

      $this->derivatives[$name] = clone $base_plugin_definition;
      $this->derivatives[$name]->set('entity_bundle', $name);
      $this->derivatives[$name]->set('label', $this->t('Group commerce order (@type)', ['@type' => $label]));
      $this->derivatives[$name]->set('description', $this->t('Adds %type commerce orders to groups both publicly and privately.', ['%type' => $label]));
    }

    return $this->derivatives;
  }

}
