<?php

namespace Drupal\gcommerce_order\EventSubscriber;

use Drupal\commerce_cart\Event\CartEntityAddEvent;
use Drupal\commerce_cart\Event\CartEvents;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\group\GroupMembershipLoaderInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Add to cart EventSubscriber for groups.
 */
class GroupCartEventSubscriber implements EventSubscriberInterface {

  /**
   * The membership loader service.
   *
   * @var \Drupal\group\GroupMembershipLoaderInterface
   */
  protected $membershipLoader;

  /**
   * The gcommerce settings configuration.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * Constructs a GroupCartEventSubscriber object.
   *
   * @param \Drupal\group\GroupMembershipLoaderInterface $membership_loader
   *   The group membership loader service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(GroupMembershipLoaderInterface $membership_loader, ConfigFactoryInterface $config_factory) {
    $this->membershipLoader = $membership_loader;
    $this->config = $config_factory->get('gcommerce.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [CartEvents::CART_ENTITY_ADD => 'onCartEntityAdd'];
  }

  /**
   * Add new cart entity to group relation for user groups.
   *
   * @param \Drupal\commerce_cart\Event\CartEntityAddEvent $event
   *   The add to cart event.
   */
  public function onCartEntityAdd(CartEntityAddEvent $event): void {
    $cart = $event->getCart();
    $user = $cart->getCustomer();
    $user_groups = $this->membershipLoader->loadByUser($user);

    // Allowed groups for which you can add a cart entity.
    $allowed_group_type_ids = $this->config->get('allowed_group_types') ?? [];
    foreach ($user_groups as $membership) {
      $group = $membership->getGroup();
      $group_type = $group->getGroupType();
      if (!in_array($group_type->id(), $allowed_group_type_ids, TRUE)) {
        continue;
      }

      // Add the entity of the cart to the groups, if it has not been added yet.
      $plugin_id = 'group_order:' . $cart->bundle();
      if ($group_type->hasPlugin($plugin_id) && empty($group->getRelationshipsByEntity($cart, $plugin_id))) {
        $group->addRelationship($cart, $plugin_id);
      }
    }
  }

}
