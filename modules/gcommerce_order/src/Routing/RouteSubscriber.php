<?php

namespace Drupal\gcommerce_order\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Subscriber for Group Commerce Order routes.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('entity.group_relationship.create_page')) {
      $copy = clone $route;
      $copy->setPath('group/{group}/orders/create');
      $copy->setDefault('base_plugin_id', 'group_order');
      $collection->add('entity.group_relationship.group_order_create_page', $copy);
    }

    if ($route = $collection->get('entity.group_relationship.add_page')) {
      $copy = clone $route;
      $copy->setPath('group/{group}/orders/add');
      $copy->setDefault('base_plugin_id', 'group_order');
      $collection->add('entity.group_relationship.group_order_add_page', $copy);
    }
  }

}
