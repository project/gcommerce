<?php

namespace Drupal\Tests\gcommerce_store\Kernel;

use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;

/**
 * Tests that all config provided by this module passes validation.
 *
 * @group gcommerce_store
 */
class GroupStoreConfigTest extends EntityKernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'address',
    'commerce',
    'commerce_price',
    'commerce_store',
    'entity',
    'flexible_permissions',
    'gcommerce',
    'gcommerce_store',
    'group',
    'options',
    'variationcache',
    'views',
  ];

  /**
   * Tests that the module's config installs properly.
   */
  public function testConfig(): void {
    $this->installConfig(['gcommerce_store']);
  }

}
