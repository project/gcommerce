<?php

namespace Drupal\Tests\gcommerce_store\Functional;

use Drupal\Tests\group\Functional\EntityOperationsTest as GroupEntityOperationsTest;

/**
 * Tests that entity operations (do not) show up on the group overview.
 *
 * @see gcommerce_store_entity_operation()
 *
 * @group gcommerce_store
 */
class EntityOperationsTest extends GroupEntityOperationsTest {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['gcommerce_store'];

  /**
   * {@inheritdoc}
   */
  public function provideEntityOperationScenarios() {
    $scenarios['withoutAccess'] = [
      [],
      ['group/1/stores' => 'Stores'],
    ];

    $scenarios['withAccess'] = [
      ['group/1/stores' => 'Stores'],
      [],
      [
        'view group',
        'access group_store overview',
      ],
    ];

    $scenarios['withAccessAndViews'] = [
      ['group/1/stores' => 'Stores'],
      [],
      [
        'view group',
        'access group_store overview',
      ],
      ['views'],
    ];

    return $scenarios;
  }

}
