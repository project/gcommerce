<?php

namespace Drupal\gcommerce_store\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Subscriber for Group Commerce Store routes.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('entity.group_relationship.create_page')) {
      $copy = clone $route;
      $copy->setPath('group/{group}/stores/create');
      $copy->setDefault('base_plugin_id', 'group_store');
      $collection->add('entity.group_relationship.group_store_create_page', $copy);
    }
    if ($route = $collection->get('entity.group_relationship.add_page')) {
      $copy = clone $route;
      $copy->setPath('group/{group}/stores/add');
      $copy->setDefault('base_plugin_id', 'group_store');
      $collection->add('entity.group_relationship.group_store_add_page', $copy);
    }
  }

}
