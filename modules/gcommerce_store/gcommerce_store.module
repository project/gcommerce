<?php

/**
 * @file
 * Enables Group functionality for the Commerce Stores.
 */

use Drupal\commerce_store\Entity\StoreTypeInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;
use Drupal\group\Entity\GroupInterface;
use Symfony\Component\Routing\RouterInterface;

/**
 * Implements hook_ENTITY_TYPE_insert().
 */
function gcommerce_store_commerce_store_type_insert(StoreTypeInterface $store_type) {
  \Drupal::service('group_relation_type.manager')->clearCachedDefinitions();
}

/**
 * Implements hook_entity_operation().
 */
function gcommerce_store_entity_operation(EntityInterface $entity) {
  $operations = [];

  if ($entity->getEntityTypeId() === 'group' && \Drupal::moduleHandler()->moduleExists('views')) {
    assert($entity instanceof GroupInterface);
    if ($entity->hasPermission('access group_store overview', \Drupal::currentUser())) {
      $router = \Drupal::service('router.no_access_checks');
      assert($router instanceof RouterInterface);
      if ($router->getRouteCollection()->get('view.group_stores.page_1') !== NULL) {
        $operations['stores'] = [
          'title' => t('Stores'),
          'weight' => 20,
          'url' => Url::fromRoute('view.group_stores.page_1', ['group' => $entity->id()]),
        ];
      }
    }
  }

  return $operations;
}
