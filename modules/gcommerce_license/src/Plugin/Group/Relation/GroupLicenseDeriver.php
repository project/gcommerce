<?php

namespace Drupal\gcommerce_license\Plugin\Group\Relation;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\group\Plugin\Group\Relation\GroupRelationTypeInterface;

/**
 * Provides a deriver for group_license.
 */
class GroupLicenseDeriver extends DeriverBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    assert($base_plugin_definition instanceof GroupRelationTypeInterface);
    $this->derivatives = [];

    /** @var \Drupal\commerce_license\LicenseTypeManager $licenseTypeManager */
    $licenseTypeManager = \Drupal::service('plugin.manager.commerce_license_type');
    $license_types = $licenseTypeManager->getDefinitions();
    foreach ($license_types as $bundle => $license_type) {
      $label = $license_type['label'];
      $this->derivatives[$bundle] = clone $base_plugin_definition;
      $this->derivatives[$bundle]->set('entity_bundle', $bundle);
      $this->derivatives[$bundle]->set('label', $this->t('Group commerce license (@type)', ['@type' => $label]));
      $this->derivatives[$bundle]->set('description', $this->t('Adds %type commerce licenses to groups both publicly and privately.', ['%type' => $label]));
    }

    return $this->derivatives;
  }

}
