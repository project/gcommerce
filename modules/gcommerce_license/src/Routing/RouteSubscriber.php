<?php

namespace Drupal\gcommerce_license\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Subscriber for Group Commerce License routes.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('entity.group_relationship.create_page')) {
      $copy = clone $route;
      $copy->setPath('group/{group}/licenses/create');
      $copy->setDefault('base_plugin_id', 'group_license');
      $collection->add('entity.group_relationship.group_license_create_page', $copy);
    }

    if ($route = $collection->get('entity.group_relationship.add_page')) {
      $copy = clone $route;
      $copy->setPath('group/{group}/licenses/add');
      $copy->setDefault('base_plugin_id', 'group_license');
      $collection->add('entity.group_relationship.group_license_add_page', $copy);
    }
  }

}
