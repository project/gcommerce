<?php

namespace Drupal\Tests\gcommerce_license\Functional;

use Drupal\Tests\group\Functional\EntityOperationsTest as GroupEntityOperationsTest;

/**
 * Tests that entity operations (do not) show up on the group overview.
 *
 * @see gcommerce_license_entity_operation()
 *
 * @requires module commerce_license
 * @group gcommerce_license
 */
class EntityOperationsTest extends GroupEntityOperationsTest {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['gcommerce_license', 'commerce_license'];

  /**
   * {@inheritdoc}
   */
  public function provideEntityOperationScenarios() {
    $scenarios['withoutAccess'] = [
      [],
      ['group/1/licenses' => 'Licenses'],
    ];

    $scenarios['withAccess'] = [
      ['group/1/licenses' => 'Licenses'],
      [],
      [
        'view group',
        'access group_license overview',
      ],
    ];

    $scenarios['withAccessAndViews'] = [
      ['group/1/licenses' => 'Licenses'],
      [],
      [
        'view group',
        'access group_license overview',
      ],
      ['views'],
    ];

    return $scenarios;
  }

}
