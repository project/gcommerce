<?php

namespace Drupal\Tests\gcommerce_license\Kernel;

use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;

/**
 * Tests that all config provided by this module passes validation.
 *
 * @requires module commerce_license
 * @group gcommerce_license
 */
class GroupLicenseConfigTest extends EntityKernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'address',
    'commerce',
    'commerce_license',
    'commerce_order',
    'commerce_price',
    'commerce_product',
    'commerce_store',
    'entity',
    'entity_reference_revisions',
    'flexible_permissions',
    'gcommerce',
    'gcommerce_license',
    'group',
    'options',
    'profile',
    'state_machine',
    'variationcache',
    'views',
  ];

  /**
   * Tests that the module's config installs properly.
   */
  public function testConfig(): void {
    $this->installConfig(['gcommerce_license']);
  }

}
