<?php

namespace Drupal\gcommerce_invoice\Plugin\Group\Relation;

use Drupal\commerce_invoice\Entity\InvoiceType;
use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\group\Plugin\Group\Relation\GroupRelationTypeInterface;

/**
 * Provides a deriver for group_invoice.
 */
class GroupInvoiceDeriver extends DeriverBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    assert($base_plugin_definition instanceof GroupRelationTypeInterface);
    $this->derivatives = [];

    foreach (InvoiceType::loadMultiple() as $name => $entity_type) {
      $label = $entity_type->label();

      $this->derivatives[$name] = clone $base_plugin_definition;
      $this->derivatives[$name]->set('entity_bundle', $name);
      $this->derivatives[$name]->set('label', $this->t('Group commerce invoice (@type)', ['@type' => $label]));
      $this->derivatives[$name]->set('description', $this->t('Adds %type commerce invoices to groups both publicly and privately.', ['%type' => $label]));
    }

    return $this->derivatives;
  }

}
