<?php

namespace Drupal\gcommerce_invoice\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Subscriber for Group Commerce Invoice routes.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('entity.group_relationship.create_page')) {
      $copy = clone $route;
      $copy->setPath('group/{group}/invoices/create');
      $copy->setDefault('base_plugin_id', 'group_invoice');
      $collection->add('entity.group_relationship.group_invoice_create_page', $copy);
    }

    if ($route = $collection->get('entity.group_relationship.add_page')) {
      $copy = clone $route;
      $copy->setPath('group/{group}/invoices/add');
      $copy->setDefault('base_plugin_id', 'group_invoice');
      $collection->add('entity.group_relationship.group_invoice_add_page', $copy);
    }
  }

}
