<?php

namespace Drupal\Tests\gcommerce_invoice\Functional;

use Drupal\Tests\group\Functional\EntityOperationsTest as GroupEntityOperationsTest;

/**
 * Tests that entity operations (do not) show up on the group overview.
 *
 * @see gcommerce_invoice_entity_operation()
 *
 * @requires module commerce_invoice
 * @group gcommerce_invoice
 */
class EntityOperationsTest extends GroupEntityOperationsTest {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['gcommerce_invoice', 'commerce_invoice'];

  /**
   * {@inheritdoc}
   */
  public function provideEntityOperationScenarios() {
    $scenarios['withoutAccess'] = [
      [],
      ['group/1/invoices' => 'Invoices'],
    ];

    $scenarios['withAccess'] = [
      ['group/1/invoices' => 'Invoices'],
      [],
      [
        'view group',
        'access group_invoice overview',
      ],
    ];

    $scenarios['withAccessAndViews'] = [
      ['group/1/invoices' => 'Invoices'],
      [],
      [
        'view group',
        'access group_invoice overview',
      ],
      ['views'],
    ];

    return $scenarios;
  }

}
