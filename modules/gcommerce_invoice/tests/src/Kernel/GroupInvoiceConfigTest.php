<?php

namespace Drupal\Tests\gcommerce_invoice\Kernel;

use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;

/**
 * Tests that all config provided by this module passes validation.
 *
 * @requires module commerce_invoice
 * @group gcommerce_invoice
 */
class GroupInvoiceConfigTest extends EntityKernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'address',
    'commerce',
    'commerce_invoice',
    'commerce_order',
    'commerce_price',
    'commerce_store',
    'entity',
    'entity_print',
    'entity_reference_revisions',
    'file',
    'flexible_permissions',
    'gcommerce',
    'gcommerce_invoice',
    'group',
    'options',
    'profile',
    'state_machine',
    'variationcache',
    'views',
  ];

  /**
   * Tests that the module's config installs properly.
   */
  public function testConfig(): void {
    $this->installConfig(['gcommerce_invoice']);
  }

}
