<?php

namespace Drupal\gcommerce_profile\Plugin\Group\Relation;

use Drupal\Core\Form\FormStateInterface;
use Drupal\group\Plugin\Group\Relation\GroupRelationBase;

/**
 * Provides a group relation type for profiles.
 *
 * @GroupRelationType(
 *   id = "group_profile",
 *   label = @Translation("Group profile"),
 *   description = @Translation("Adds profiles to groups both publicly and privately."),
 *   entity_type_id = "profile",
 *   entity_access = TRUE,
 *   reference_label = @Translation("Title"),
 *   reference_description = @Translation("The title of the profile to add to the group"),
 *   deriver = "Drupal\gcommerce_profile\Plugin\Group\Relation\GroupProfileDeriver",
 * )
 */
class GroupProfile extends GroupRelationBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $config = parent::defaultConfiguration();
    $config['entity_cardinality'] = 1;
    $config['addressbook_list_group_profiles'] = FALSE;
    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    // Disable the entity cardinality field as the functionality of this module
    // relies on a cardinality of 1. We don't just hide it, though, to keep a UI
    // that's consistent with other group relations.
    $info = $this->t("This field has been disabled by the plugin to guarantee the functionality that's expected of it.");
    $form['entity_cardinality']['#disabled'] = TRUE;
    $form['entity_cardinality']['#description'] .= '<br /><em>' . $info . '</em>';

    $form['addressbook_list_group_profiles'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Addressbook: List only group profiles'),
      '#description' => $this->t('Whether the addressbook should list only customer profiles belonging to groups the customer is a member of.'),
      '#default_value' => $this->configuration['addressbook_list_group_profiles'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function calculateDependencies() {
    $dependencies = parent::calculateDependencies();
    $dependencies['config'][] = 'profile.profile_type.' . $this->getRelationType()->getEntityBundle();
    return $dependencies;
  }

}
