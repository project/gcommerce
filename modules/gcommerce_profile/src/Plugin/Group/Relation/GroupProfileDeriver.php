<?php

namespace Drupal\gcommerce_profile\Plugin\Group\Relation;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\group\Plugin\Group\Relation\GroupRelationTypeInterface;
use Drupal\profile\Entity\ProfileType;

/**
 * Provides a deriver for group_profile.
 */
class GroupProfileDeriver extends DeriverBase {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    assert($base_plugin_definition instanceof GroupRelationTypeInterface);
    $this->derivatives = [];

    foreach (ProfileType::loadMultiple() as $name => $entity_type) {
      $label = $entity_type->label();

      $this->derivatives[$name] = clone $base_plugin_definition;
      $this->derivatives[$name]->set('entity_bundle', $name);
      $this->derivatives[$name]->set('label', $this->t('Group profile (@type)', ['@type' => $label]));
      $this->derivatives[$name]->set('description', $this->t('Adds %type profiles to groups both publicly and privately.', ['%type' => $label]));
    }

    return $this->derivatives;
  }

}
