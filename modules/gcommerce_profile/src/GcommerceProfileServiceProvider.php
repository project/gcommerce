<?php

namespace Drupal\gcommerce_profile;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Group commerce service provider.
 */
class GcommerceProfileServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container) {
    // We cannot use the module handler as the container is not yet compiled.
    // @see \Drupal\Core\DrupalKernel::compileContainer()
    $modules = $container->getParameter('container.modules');

    // Swap the addressbook service with our own if commerce_order is enabled.
    if (isset($modules['commerce_order'])) {
      $container->getDefinition('commerce_order.address_book')
        ->setClass(AddressBook::class)
        ->addArgument(new Reference('group.membership_loader'))
        ->addArgument(new Reference('config.factory'));
    }
  }

}
