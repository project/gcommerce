<?php

namespace Drupal\gcommerce_profile\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Subscriber for Group Profile routes.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('entity.group_relationship.create_page')) {
      $copy = clone $route;
      $copy->setPath('group/{group}/profiles/create');
      $copy->setDefault('base_plugin_id', 'group_profile');
      $collection->add('entity.group_relationship.group_profile_create_page', $copy);
    }

    if ($route = $collection->get('entity.group_relationship.add_page')) {
      $copy = clone $route;
      $copy->setPath('group/{group}/profiles/add');
      $copy->setDefault('base_plugin_id', 'group_profile');
      $collection->add('entity.group_relationship.group_profile_add_page', $copy);
    }
  }

}
