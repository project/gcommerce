<?php

namespace Drupal\gcommerce_profile;

use Drupal\commerce_order\AddressBook as CoreAddressBook;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeBundleInfo;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\group\GroupMembershipLoaderInterface;
use Drupal\profile\Entity\ProfileInterface;
use Drupal\user\UserInterface;

/**
 * Extends the core address book.
 */
class AddressBook extends CoreAddressBook {

  /**
   * The membership loader service.
   *
   * @var \Drupal\group\GroupMembershipLoaderInterface
   */
  protected $membershipLoader;

  /**
   * The gcommerce settings configuration.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * Constructs a new AddressBook object.
   *
   * @param \Drupal\Core\Entity\EntityTypeBundleInfo $entity_type_bundle_info
   *   The entity type bundle info.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\group\GroupMembershipLoaderInterface $membership_loader
   *   The group membership loader service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(EntityTypeBundleInfo $entity_type_bundle_info, EntityTypeManagerInterface $entity_type_manager, GroupMembershipLoaderInterface $membership_loader, ConfigFactoryInterface $config_factory) {
    parent::__construct($entity_type_bundle_info, $entity_type_manager);

    $this->membershipLoader = $membership_loader;
    $this->config = $config_factory->get('gcommerce.settings');
  }

  /**
   * {@inheritdoc}
   */
  public function loadAll(UserInterface $customer, $profile_type_id, array $available_countries = []) {
    $profiles = [];
    $list_only_group_profiles = FALSE;
    // Load the profiles belonging to groups the given customer is member of.
    foreach ($this->membershipLoader->loadByUser($customer) as $group_membership) {
      $group = $group_membership->getGroup();
      $plugin_id = "group_profile:$profile_type_id";
      if (!$group->getGroupType()->hasPlugin($plugin_id)) {
        continue;
      }
      // Check whether only profiles belonging to groups should be returned
      // by the Addressbook.
      $configuration = $group->getGroupType()->getPlugin($plugin_id)->getConfiguration();
      // @todo Check what to do when profiles from different group types
      // with different configuration is returned.
      if (!empty($configuration['addressbook_list_group_profiles'])) {
        $list_only_group_profiles = TRUE;
      }
      foreach ($group->getRelatedEntities("group_profile:$profile_type_id") as $profile) {
        if (!$this->isAvailable($profile, $available_countries)) {
          continue;
        }
        $profiles[$profile->id()] = $profile;
      }
    }

    // Skip returning profiles belonging to the current customer if configured
    // to do so.
    if (!$list_only_group_profiles) {
      $profiles += parent::loadAll(
        $customer,
        $profile_type_id,
        $available_countries
      );
    }

    return $profiles;
  }

  /**
   * {@inheritdoc}
   */
  public function copy(ProfileInterface $profile, UserInterface $customer) {
    parent::copy($profile, $customer);
    // Add the addressbook profile to customer's groups.
    if ($profile->getData('address_book_profile_id')) {
      $addressbook_profile = $this->profileStorage->load($profile->getData('address_book_profile_id'));

      $allowed_group_type_ids = $this->config->get('allowed_group_types') ?? [];

      $groups = $this->membershipLoader->loadByUser($customer);
      foreach ($groups as $membership) {
        $group = $membership->getGroup();
        $groupType = $group->getGroupType();
        if (!in_array($groupType->id(), $allowed_group_type_ids, TRUE)) {
          continue;
        }
        $plugin_id = 'group_profile:' . $addressbook_profile->bundle();
        if ($groupType->hasPlugin($plugin_id)
          && empty($group->getRelationshipsByEntity($addressbook_profile, $plugin_id))) {
          $group->addRelationship($profile, $plugin_id);
        }
      }
    }
  }

}
