<?php

namespace Drupal\Tests\gcommerce_profile\Functional;

use Drupal\Tests\group\Functional\EntityOperationsTest as GroupEntityOperationsTest;

/**
 * Tests that entity operations (do not) show up on the group overview.
 *
 * @see gcommerce_profile_entity_operation()
 *
 * @group gcommerce_profile
 */
class EntityOperationsTest extends GroupEntityOperationsTest {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['gcommerce_profile'];

  /**
   * {@inheritdoc}
   */
  public function provideEntityOperationScenarios() {
    $scenarios['withoutAccess'] = [
      [],
      ['group/1/profiles' => 'Profiles'],
    ];

    $scenarios['withAccess'] = [
      ['group/1/profiles' => 'Profiles'],
      [],
      [
        'view group',
        'access group_profile overview',
      ],
    ];

    $scenarios['withAccessAndViews'] = [
      ['group/1/profiles' => 'Profiles'],
      [],
      [
        'view group',
        'access group_profile overview',
      ],
      ['views'],
    ];

    return $scenarios;
  }

}
