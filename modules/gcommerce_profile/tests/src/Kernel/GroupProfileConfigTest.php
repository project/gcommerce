<?php

namespace Drupal\Tests\gcommerce_profile\Kernel;

use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;

/**
 * Tests that all config provided by this module passes validation.
 *
 * @group gcommerce_profile
 */
class GroupProfileConfigTest extends EntityKernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'address',
    'commerce',
    'commerce_price',
    'commerce_store',
    'entity',
    'flexible_permissions',
    'gcommerce',
    'gcommerce_profile',
    'group',
    'options',
    'profile',
    'variationcache',
    'views',
  ];

  /**
   * Tests that the module's config installs properly.
   */
  public function testConfig(): void {
    $this->installConfig(['gcommerce_profile']);
  }

}
